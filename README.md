<div align="center">
  <a href="https://ambtec.com" title="ambtec" target="_blank">
    <img src="./assets/ambtec-logo-gitlab.png" width="128" />
  </a>
  <p><i>create shape improve</i></p>
</div>

<hr />

# Infrastructure Demonstration

1. [*Low Code Orchestration*](#Orchestrator-Service) with [Node-Red](https://nodered.org/) for better maintenance and management.
   - *HTTP endpoint management* for REST communication (ideal to connect to any Web Service or IoT device).
   - *SocketIo endpoint management* for neartime communication.
   - *[OpenAPI](https://swagger.io/specification/) support* for standardized endpoint documentation (HTTP and SocketIo)
1. [*IAM flow integration*](#IAM-Service) via [Keycloak](https://www.keycloak.org/) for standardized access and permission control.
   - Provide options to integrate external Identity Broker
1. [*Event Stream support*](#Event-Stream-support) for high system performance
   - Provide high flexibility and scaling via a loose service binding
2. *High infrastructure flexibility* for lose host binding.
   - Build with simplicity in mind to be hosted on a wide range of container driven infrastructures
   - Keeps complexity low while enables you to change your host provider (if you need to)

![Domainlevel](./assets/Infrastructure-v2-domainlevel.drawio.png "Domainlevel") \
(Made with https://app.diagrams.net/)

## Components
### Client

#### [Project Page](https://gitlab.com/ambtec/project-page)

Localhost URL: http://project-page.localhost

The project page works for presentation and demonstration purposes. It demonstrate the integration of a Widget (Dashboard Widget) into the page context.

#### [Dashboard Widget](https://gitlab.com/ambtec/dashboard-widget)

Localhost URL: http://dashboard-widget.localhost

Demonstrate the functional workflow of a web component to interacte with the IAM and Orchestrator Service to send and retrieve messages. Can be used with a couple of users for a easy chat.

> **Note:** \
> In case of a ` 3rd party check iframe ` error make sure to disable any adblocker for dashboard-widget.localhost


### Infrastucture
<a name="Orchestrator-Service"></a>
#### [Orchestrator Service](https://gitlab.com/ambtec/orchestrator-service)

Localhost URL: http://orchestrator.localhost/admin

A [Node-Red](https://nodered.org/) setup to showcase flows for socket and http authentication and messages.

<a name="IAM-Service"></a>
#### [IAM Service](https://gitlab.com/ambtec/iam-service)

Localhost URL: http://iam.localhost

A Identitiy Access Management System based on keycloak. Default setup is used withou customizations. Seamless Login/Logout integration to external environments (see Client Demo).

<a name="Event-Stream-support"></a>
#### Event Stream

Kafka Control Center is available on Default URL: http://localhost:9021

---
## Directory Structure

```
├─ config
│   └─ /certs                     // localhost certificate
├─ docker
│   ├─ /dashboard-widget_service  // nginx configuration
│   ├─ /iam-db_service            // database configuration
│   └─ /orchestrator_service      // node-red configuration
│   └─ /project-page_service      // nginx configuration
│   └─ /proxy_service             // proxy server configuration
├─ temp                           // temporary storage
└─ _docker-certs.sh               // helper script to create localhost certificates
└─ _docker-down.sh                // helper script to shut down the running environment and to clean the system
└─ _docker-install.sh             // helper script to prepare env configurations
└─ _docker-up.sh                  // helper script to start up the environment
├─ .env                           // local environment variables which are not versioned
└─ docker-compose.yml             // container orchestration
```

---
## Prepare environment

Prepare your local environment

1. [Configurate ENV's](#envs)
2. [Add certificate](#certs)

<a name="envs"></a>

### ENV's

You can run ` _docker-install.sh ` which will help to create the ENV configuration automatically.

Available environment options. Marked options (\*) can be edited freely.

#### General

- `PROJECT` \*= [string] Project name
- `PRODUCTION` = `false`

#### Repository settings

- `REPO_EMAIL` = [string] Only required if to connect to a private repository
- `REPO_USERNAME` = [string] Only required if to connect to a private repository
- `REPO_PASSWORD` = [string] Only required if to connect to a private repository

#### IAM settings

- `KEYCLOAK_VERSION` = `18.0.0`
- `KEYCLOAK_USER` \*= [string] "username"
- `KEYCLOAK_PASSWORD` \*= [string] "password"
- `IAM_SERVICE_HOST` = `iam.localhost`
- `IAM_DEFAULT_CLIENT_ID` = `account`
- `IAM_ADDRESS_TOKEN` = `/auth/realms/ambtec/protocol/openid-connect/token`
- `IAM_ADDRESS_LOGOUT` = `/auth/realms/ambtec/protocol/openid-connect/logout`

#### IAM database settings

- `IAM_USER` \*= [string] "username"
- `IAM_PASS` \*= [string] "password"
- `IAM_DB` = `example-iam-db`

#### Orchestrator settings

- `ORCHESTRATOR_ENDPOINT` = `http://orchestrator.localhost`
- `ORCHESTRATOR_MAIN_PROJECT_ID` = `orchestrator-flows`
- `ORCHESTRATOR_MAIN_PROJECT_PATH` = `doc`

#### Orchestrator socket settings

- `SOCKETIO_CORS_ORIGINS` = `http://project-page.localhost,http://dashboard-widget.localhost`
- `SOCKETIO_CORS_HEADERS` = ` `
- `SOCKETIO_CORS_CREDENTIALS` = `true`
- `SOCKETIO_ENDPOINT` = `/socket.io`
- `SOCKETIO_EVENTS` = `authenticate,open,error,close,ping,packet,reconnect_attempt,reconnect,reconnect_error,reconnect_failed,connect,connect_error,disconnect,login,logout,testAll,testEmit,testBroadcast,joinRoom,sendRoom,leaveRoom,rooms,joinServiceEvent,leaveServiceEvent`

<a name="certs"></a>
### Certificate

Use mkcert to create your root certificates first. 

To od so go to [mkcert](https://github.com/FiloSottile/mkcert) first and follow the installation guide for your OS.

Next check installation is complete while enter ` mkcert ` in your cli. If you receive an mkcert output everything is ok.

Then you can use the helper script ` _docker_certs.sh ` at the document root to create and place your localhost certificates at ` ./config/certs `.

Your certificates are ready.

---

## Start up environment

Run ` _docker_up.sh ` to start up the infrastucture example. This script will prepare all required configuration and create a temporary ` temp_docker-compose.yml ` file to start up docker. 

The purpose of the ` temp_docker-compose.yml ` file is to manage private repository access. The startup script will replace all ` ${...} ` with corresponding ENV keys to provide them as arguments so the repository can be accessed and cloned.

```
   container-name:
      build: https://${REPO_USERNAME}:${REPO_PASSWORD}@gitlab.com/ambtec/iam-service.git#v0.1.0 
```

## Shut down environment

Run ` _docker_down.sh ` to shut down the environment. 

> **Note:** \
> You will be ask if you like to prune container and volume caches. This might be handy to prevent caching issues while doing development and to keep the system clean. But be aware you will lose all stored container data like stored database informations, etc. 

## License
MIT
