// include dependencies
const express = require("express");
const { createProxyMiddleware } = require("http-proxy-middleware");

const port = 80;

// proxy middleware options
/** @type {import('http-proxy-middleware/dist/types').Options} */
const options = {
  ws: true, // proxy websockets
  router: {
    "orchestrator.localhost": "http://orchestrator.localhost",
    "iam.localhost": "http://iam.localhost",
    "iam-db.localhost": "http://iam-db:3306",
    "iam-widget.localhost": "http://iam-widget.localhost",
    "dashboard-widget.localhost": "http://dashboard-widget.localhost",
    "project-page.localhost": "http://project-page.localhost",
  },
};

// create the proxy (without context)
const developmentProxy = createProxyMiddleware(options);

const app = express();
app.use("/", developmentProxy);
app.listen(port);
