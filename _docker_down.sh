#!/bin/sh

echo "Stopping all docker instances"
docker-compose down

echo "Clean configuration"
file="docker-compose-tmp.yml"
[ -f file ] && rm file
rm -rf orchestrator_service-tmp

echo "Clean system and volumes"
docker system prune
docker volume prune