#!/bin/bash

echo "The ENV helper will prepare the configuration for you"

replace() {
  case "$OSTYPE" in
    solaris*) sed -i "$1" $2 ;;
    darwin*)  sed -i "" "$1" $2 ;;
    linux*)   sed -i "$1" $2 ;;
    bsd*)     sed -i "$1" $2 ;;
    msys*)    sed -i "$1" $2 ;;
    cygwin*)  sed -i "$1" $2 ;;
    *)        sed -i "$1" $2 ;;
  esac
}

# Ask for parameter username
read -p "Plz enter a demo username: " USERNAME

# Ask for parameter password
read -p "Plz enter a demo password: " PASSWORD

EMAIL="${USERNAME}@any.mail"
DATABASE="example-iam-db"

cp -r .env.dist .env

replace "s/REPO_EMAIL=/REPO_EMAIL=$EMAIL/g" .env
replace "s/REPO_USERNAME=/REPO_USERNAME=$USERNAME/g" .env
replace "s/REPO_PASSWORD=/REPO_PASSWORD=$PASSWORD/g" .env
replace "s/KEYCLOAK_USER=/KEYCLOAK_USER=$USERNAME/g" .env
replace "s/KEYCLOAK_PASSWORD=/KEYCLOAK_PASSWORD=$PASSWORD/g" .env
replace "s/IAM_USER=/IAM_USER=$USERNAME/g" .env
replace "s/IAM_PASS=/IAM_PASS=$PASSWORD/g" .env
replace "s/IAM_DB=/IAM_DB=$DATABASE/g" .env

echo "Your ENV's are prepared and ready to use"
echo "Remember to use '${USERNAME}:${PASSWORD}' for IAM-Service (Keycloak) Admin Backend login"