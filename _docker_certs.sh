#!/bin/bash

echo "Create localhost certificates"

rm -rf ./config/certs
mkdir ./config/certs

cd ./config/certs

mkcert -install
mkcert -key-file localhost-key.pem -cert-file localhost.pem localhost *.localhost iam.localhost iam-db.localhost orchestrator.localhost dashboard-widget.localhost project-page.localhost 192.0.0.1 ::1

echo "Certificates ready for localhost"