#!/bin/bash

echo "Collect and validate ENV's"

read_var() {
  VAR=$(grep -s $1 $2 | xargs)
  IFS="=" read -ra VAR <<< "$VAR"
  echo ${VAR[1]}
}

validate_envs() {
  CHECKS_PASSED=0
  if [[ ! -z $(read_var REPO_EMAIL .env) ]]; then
    CHECKS_PASSED=$(($CHECKS_PASSED + 1))
  fi
  if [[ ! -z $(read_var REPO_USERNAME .env) ]]; then
    CHECKS_PASSED=$(($CHECKS_PASSED + 1))
  fi
  if [[ ! -z $(read_var REPO_PASSWORD .env) ]]; then
    CHECKS_PASSED=$(($CHECKS_PASSED + 1))
  fi
  echo ${CHECKS_PASSED}
}

replace() {
  case "$OSTYPE" in
    solaris*) sed -i "$1" $2 ;;
    darwin*)  sed -i "" "$1" $2 ;;
    linux*)   sed -i "$1" $2 ;;
    bsd*)     sed -i "$1" $2 ;;
    msys*)    sed -i "$1" $2 ;;
    cygwin*)  sed -i "$1" $2 ;;
    *)        sed -i "$1" $2 ;;
  esac
}

### Validate ENV's, if not set run ENV helper first
if [[ $(validate_envs) < 3 ]]; then
  echo "ENV's not found or incomplete"
  sh ./env.sh
fi

# Fetch ENV's
REPO_EMAIL=$(read_var REPO_EMAIL .env)
REPO_USERNAME=$(read_var REPO_USERNAME .env)
REPO_PASSWORD=$(read_var REPO_PASSWORD .env)

### Run build

echo "Prepare start up configuration"

# Recreate fresh temp folder
rm -rf ./temp
mkdir ./temp

# Recreate fresh iam-db_service/mysql folder
rm -rf ./docker/iam-db_service/mysql
mkdir ./docker/iam-db_service/mysql

# Clone configuration folder
cp -rf ./docker/orchestrator_service ./temp/orchestrator_service-tmp

replace "s/\${REPO_USERNAME}/$REPO_USERNAME/g" ./temp/orchestrator_service-tmp/.config.users.json
replace "s/\${REPO_EMAIL}/$REPO_EMAIL/g" ./temp/orchestrator_service-tmp/.config.users.json

# Clone original compose configurations
cp -f docker-compose.yml ./temp_docker-compose.yml

replace "s/\${REPO_USERNAME}/$REPO_USERNAME/g" ./temp_docker-compose.yml
replace "s/\${REPO_PASSWORD}/$REPO_PASSWORD/g" ./temp_docker-compose.yml
replace "s/\/orchestrator_service\//\/orchestrator_service-tmp\//g" ./temp_docker-compose.yml

echo "Start Docker environment"

# Build docker with build arguments
docker-compose -f ./temp_docker-compose.yml build --build-arg REPO_USERNAME="$REPO_USERNAME" --build-arg REPO_PASSWORD="$REPO_PASSWORD"

# Startup docker
docker-compose -f ./temp_docker-compose.yml up